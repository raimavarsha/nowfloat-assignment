/*
 Author: Varsha Kumari
 date: 16/12/2018
*/
var current_page = 1;
var records_per_page = 4;
var originalData = [];
var datas = [];
var listing_table;

/* Document ready*/
document.addEventListener("DOMContentLoaded", handler)


/* callback function once documents content is loaded */
function handler() {
    document.getElementById('quick_search').addEventListener('keyup', filterMovieItems);
    $.ajax({
        method: 'GET',
        url: './assets/data/movies.json'
    }).then(function (response) {
        originalData = response;
        appendData(response);
    })
}

/* append data */
function appendData(data) {
    datas = Object.assign([], data);
    changePage(1);
}

//filter items
function filterMovieItems(event) {
    var filterData = [];
    if (event.target.value.trim() != '') {
        if (event.target.value.length > 0) {
            for (var i = 0; i < datas.length; i++) {
                if (datas[i].movieDetails.movieName.toLowerCase().indexOf(event.target.value.toLowerCase()) != -1) {
                    filterData.push(datas[i]);
                }
            }
            datas = filterData;
            if (datas.length > 0) {
                showImage(true);
            } else {
                showImage(false);
            }
        }
    } else {
        datas = originalData;
        showImage(true)
    }
};

//Show Image
function showImage(status){
    if(status){
        document.getElementById('page-section').style.display = 'block';
        changePage(1);
    }else{
        document.getElementById('page-section').style.display = 'none';
        listing_table.innerHTML = "<div class='data-not-found'>Movie Not Found</div>";
    }

}

//on click pf previous and next
function onClickPageChange(change){
    if(change == 'prev' && current_page > 1){
        current_page--;
        changePage(current_page);
    }else if(change == 'next' && current_page < numPages()){
        current_page++;
        changePage(current_page);
    }

}

//Render movies on change pf page
function changePage(page) {
    var btn_next = document.getElementById("btn_next");
    var btn_prev = document.getElementById("btn_prev");
    listing_table = document.getElementById("movies_row");
    var page_span = document.getElementById("page");

    // Validate page
    if (page < 1) page = 1;
    if (page > numPages()) page = numPages();

    listing_table.innerHTML = "";
    for (var i = (page - 1) * records_per_page; i < (page * records_per_page) && i < datas.length; i++) {
        listing_table.innerHTML += `<div class="movie_column">
                <div class="full">
                    <div class="movie-info">
                        <div class="movie_img">
                            <img src="./assets/img/${datas[i].movieThumbnail}.png" alt="img"/>
                        </div>
                        <div class="movie_detail">
                            <p>
                                <span class="movie_name">${datas[i].movieDetails.movieName}</span>
                                <br>
                                <span class="movie_year">${datas[i].movieDetails.movieRealeaseYr}</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>`
    }
    page_span.innerHTML = page + "/" + numPages();

    if (page == 1) {
        btn_prev.style.display = "none";
    } else {
        btn_prev.style.display = "block";
    }

    if (page == numPages()) {
        btn_next.style.display = "none";
    } else {
        btn_next.style.display = "block";
    }
}

//Pages
function numPages() {
    return Math.ceil(datas.length / records_per_page);
}
